
//
// Created by Михаил on 16.11.2023.
//
#pragma once
#include "image_struct.h"
#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H

#endif //IMAGE_TRANSFORMER_ROTATE_H
int rotate( struct image* source, int angle);
int rotate_90_degrees(struct image* image);
