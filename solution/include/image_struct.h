#pragma once

#include <stdint.h>
#include <stdlib.h>

#include "bmp_lib.h"


struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

extern const char* rotate_status_string[];

struct image {
    size_t width, height;
    struct pixel* data;
};

// struct image* img_mock;
int new_img(uint32_t width, uint32_t height, struct image* img);
