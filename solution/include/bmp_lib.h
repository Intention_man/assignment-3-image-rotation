#pragma once

#include <stdio.h>

#include "image_struct.h"
#include "status.h"


#ifndef IMAGE_TRANSFORMER_BMP_FORMAT_H
#define IMAGE_TRANSFORMER_BMP_FORMAT_H
#endif //IMAGE_TRANSFORMER_BMP_FORMAT_H

/*  deserializer   */
extern const char *read_status_string[];

struct image;
int from_bmp(FILE *input_file_stream, struct image *img);

/*  serializer   */

extern const char *write_status_string[];

int to_bmp(FILE *out, struct image const *img_const);

