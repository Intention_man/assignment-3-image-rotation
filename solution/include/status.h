//
// Created by Михаил on 26.12.2023.
//

#ifndef IMAGE_TRANSFORMER_STATUS_H
#define IMAGE_TRANSFORMER_STATUS_H
enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE = 1,
    READ_INVALID_BITS = 2,
    READ_INVALID_HEADER = 3,
    ALLOCATE_ERROR = 4
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR = 1
};

enum rotate_status {
    ROTATE_OK = 0,
    ROTATE_ERROR = 1
};
#endif //IMAGE_TRANSFORMER_STATUS_H
