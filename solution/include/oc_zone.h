#pragma once
#include <stdio.h>

/*  open   */
enum open_status {
  OPEN_OK = 0,
  OPEN_FAIL = 1
};

extern const char* open_status_string[];

FILE* open_file(const char* filename, const char* mode);

/*  close   */

enum close_status  {
  CLOSE_OK = 0,
  CLOSE_FAIL = 1
};

extern const char* close_status_string[];

enum close_status close_file(FILE* filename);
