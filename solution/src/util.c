//
// Created by Михаил on 15.11.2023.
//

#include "util.h"


void image_clear(struct image* image_to_destroy){
    free(image_to_destroy->data);
    image_to_destroy->data = NULL;
}
