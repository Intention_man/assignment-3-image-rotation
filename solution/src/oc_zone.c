#include "oc_zone.h"

/*  open   */

const char* open_status_string[] = {
  [OPEN_OK] = "OPEN_OK",
  [OPEN_FAIL] = "OPEN_FAIL",
};

FILE *open_file(const char *filename, const char *mode){
    if (!filename)
        return NULL;
    return fopen(filename, mode);
}

/*  close   */

const char* close_status_string[] = {
  [CLOSE_OK] = "CLOSE_OK",
  [CLOSE_FAIL] = "CLOSE_FAIL",
};

enum close_status close_file(FILE* filename){
    if (fclose(filename) == -1)
        return CLOSE_FAIL;
    return CLOSE_OK;
}
