#include "rotate.h"
#include "util.h"

/* создаёт копию изображения, которая повёрнута на 90 градусов */
int rotate(struct image *source, int angle) {
    if (angle < 0) angle += 360;
    int count = angle / 90;
    for (int i = 0; i < count; i++) {
        if (rotate_90_degrees(source) != ROTATE_OK)
            return 1;
    }
    return 0;
}

int rotate_90_degrees(struct image *image) {
    uint32_t old_width = image->width;
    uint32_t old_height = image->height;

    struct image rotated_img;
    struct image* img = &rotated_img;
    img->data = malloc(old_height * old_width * sizeof(struct pixel));
    if (img->data == NULL) {
//        free(img->data);
        image_clear(img);
        return ALLOCATE_ERROR;
    }

    img->width = old_height;
    img->height = old_width;
//    enum read_status create_status = new_img(old_height, old_width, &rotated_img);
//    if (create_status != READ_OK){
//        image_clear(image);
//        return ROTATE_ERROR;
//    }

    for (uint32_t row = 0; row < old_width; row++) {
        for (uint32_t col = 0; col < old_height; col++) {
            rotated_img.data[row * old_height + col] =
                    image->data[old_width * (old_height - 1 - col) + row];
        }
    }
    image_clear(image);
    *image = rotated_img;

    return ROTATE_OK;
}

const char *rotate_status_string[] = {
        [ROTATE_OK] = "ROTATE_OK",
        [ROTATE_ERROR] = "ROTATE_ERROR",
};
