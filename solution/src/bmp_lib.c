#include "bmp_lib.h"
#include "bmp_header.h"
#include "util.h"


#define BITS_PER_PIXEL 24
#define BF_TYPE 0x4d42


static uint32_t get_padding(uint32_t width) {
    return (-1 * (width * sizeof(struct pixel))) % 4;
}

const char *read_status_string[] = {
        [READ_OK] = "READ_OK",
        [READ_INVALID_SIGNATURE] = "READ_INVALID_SIGNATURE",
        [READ_INVALID_BITS] = "READ_INVALID_BITS",
        [READ_INVALID_HEADER] = "READ_INVALID_HEADER",
        [ALLOCATE_ERROR] = "ALLOCATE_ERROR"
};

int from_bmp(FILE *input_file_stream, struct image *img) {
    struct bmp_header header = {0};
    const size_t read_objects = fread(&header, sizeof(struct bmp_header), 1, input_file_stream);
    if (read_objects != 1)
        return READ_INVALID_HEADER;

    if (header.bfType != BF_TYPE || header.biBitCount != BITS_PER_PIXEL || header.biCompression != 0) {
        fprintf(stderr, "%u\n", header.biBitCount);
        return READ_INVALID_SIGNATURE;
    }

    fseek(input_file_stream, header.bOffBits, SEEK_SET);

    uint32_t width = header.biWidth, height = header.biHeight;
    uint32_t padding = get_padding(width);

    img->data = malloc(width * height * sizeof(struct pixel));
    if (img->data == NULL) {
        free(img->data);
        image_clear(img);
        return ALLOCATE_ERROR;
    }

    img->width = width;
    img->height = height;
//    return READ_OK;
//    enum read_status create_status = new_img(width, height, &(*img));
//    if (create_status != READ_OK){
//        image_clear(img);
//        return create_status;
//    }

    for (uint32_t row = 0; row < height; row++) {
        if (fread(&img->data[(height - row - 1) * width], sizeof(struct pixel), width, input_file_stream) !=
            width ||
            fseek(input_file_stream, padding, SEEK_CUR)) {
            free(img->data);
            return READ_INVALID_BITS;
        }
    }
    return READ_OK;
}

/*  serializer   */
const char *write_status_string[] = {
        [WRITE_OK] = "WRITE_OK",
        [WRITE_ERROR] = "WRITE_ERROR",
};

int to_bmp(FILE *output_file_stream, struct image const *img_const) {
    uint32_t width = img_const->width, height = img_const->height;
    struct bmp_header header = {
            .bfType = BF_TYPE,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = 1,
            .biBitCount = BITS_PER_PIXEL
    };
    uint32_t padding = get_padding(width);
    header.biSizeImage = (width * sizeof(struct pixel) + padding) * width;
    header.bfileSize = header.bOffBits + header.biSizeImage;

    if (fwrite(&header, sizeof(struct bmp_header), 1, output_file_stream) != 1)
        return WRITE_ERROR;

    const uint8_t zeroes[4] = {0};
    for (uint32_t row = 0; row < height; row++) {
        if (fwrite(
                &img_const->data[(height - row - 1) * width],
                sizeof(struct pixel),
                width,
                output_file_stream
        ) != width ||
            fwrite(zeroes, padding, 1, output_file_stream) != 1) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
