#include "oc_zone.h"
#include "bmp_lib.h"
#include "rotate.h"
#include "util.h"


static int throw_error(const char *mes) {
    fprintf(stderr, "%s\n", mes);
    return 1;
}

static void print_exception(const char *mes) {
    fprintf(stdin, "%s\n", mes);
}

int main(int argc, char **argv) {
    if (argc != 4)
        return throw_error("Wrong amount of arguments");

    const char *transformed_image_address = argv[2];
    int angle = atoi(argv[3]) % 360;

    // open input_file_stream
    FILE *input_file_stream = fopen(argv[1], "rb");
    if (!input_file_stream){
        free(input_file_stream);
        return throw_error(open_status_string[1]);
    }

    // read input_file_stream in structure
    struct image img = { 0 };
    int r_stat = from_bmp(input_file_stream, &img);
    if (r_stat != 0) {
        close_file(input_file_stream);
        image_clear(&img);
        return throw_error(read_status_string[r_stat]);
    }

    // close input_file_stream
    int c_stat = close_file(input_file_stream);
    if (c_stat != 0){
        image_clear(&img);
        print_exception(close_status_string[c_stat]);
    }

    // rotation
    int rot_stat = rotate(&img, angle);
    if (rot_stat != ROTATE_OK){
        image_clear(&img);
        return throw_error(rotate_status_string[rot_stat]);
    }

    // open output_file_stream
    FILE *output_file_stream = open_file(transformed_image_address, "wb");
    if (!output_file_stream){
        image_clear(&img);
        close_file(output_file_stream);
        return throw_error(open_status_string[1]);
    }

    // write structure in output_file_stream 
    int w_stat = to_bmp(output_file_stream, (struct image const *) &img);
    if (w_stat != 0){
        image_clear(&img);
        close_file(output_file_stream);
        return throw_error(write_status_string[w_stat]);
    }

    image_clear(&img);
    c_stat = close_file(output_file_stream);
    if (c_stat != 0)
        print_exception(close_status_string[c_stat]);
    return 0;
}
