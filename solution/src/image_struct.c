#include "image_struct.h"

int new_img(uint32_t width, uint32_t height, struct image *img) {
    img->data = malloc(width * height * sizeof(struct pixel));
    if (img->data == NULL) {
        free(img->data);
        return ALLOCATE_ERROR;
    }

    img->width = width;
    img->height = height;
    return READ_OK;
}
